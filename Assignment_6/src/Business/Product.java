/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Comparator;

/**
 *
 * @author sanch
 */
public class Product {
    private String prodName;
    private double ceilprice;
    private double floorPrice;
    private double targetPrice;
    private int productID;
    private int avail;
    private int initialQuantity;

    public int getInitialQuantity() {
        return initialQuantity;
    }

    public void setInitialQuantity(int initialQuantity) {
        this.initialQuantity = initialQuantity;
    }
    ArrayList<MarketOffer> marketOffers;
    private static int count =0;

    public Product() {
        count++;
        productID = count;
        marketOffers = new ArrayList<MarketOffer>();
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public double getCeilprice() {
        return ceilprice;
    }

    public void setCeilprice(double ceilprice) {
        this.ceilprice = ceilprice;
    }

    public double getFloorPrice() {
        return floorPrice;
    }

    public void setFloorPrice(double floorPrice) {
        this.floorPrice = floorPrice;
    }

    public double getTargetPrice() {
        return targetPrice;
    }

    public void setTargetPrice(double targetPrice) {
        this.targetPrice = targetPrice;
    }

    

    public ArrayList<MarketOffer> getMarketOffers() {
        return marketOffers;
    }

    public void setMarketOffers(ArrayList<MarketOffer> marketOffers) {
        this.marketOffers = marketOffers;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Product.count = count;
    }
    
    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getAvail() {
        return avail;
    }

    public void setAvail(int avail) {
        this.avail = avail;
    }

    @Override
    public String toString() {
        return String.valueOf(this.productID);
    }   
    
    public MarketOffer searchMarketOffer(Market m){
        for(MarketOffer mo : marketOffers){
            if(mo.getM().getMarketName().equals(m.getMarketName())){
                return mo;
            }
        }
        return null;
    }
    public static final Comparator<Product> DECENDING_COM = new Comparator<Product>() {

   

        @Override
        public int compare(Product t, Product t1) {
            return (int) ((t1.getInitialQuantity()- t1.getAvail())- (t.getInitialQuantity() - t.getAvail()));
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    };
    
   
       
}
