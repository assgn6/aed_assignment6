/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author sanch
 */
public class Order {
    private ArrayList<OrderItem> orderItemList;
    private int orderNumber;
    private Date date;
    private Customer customer;
    private double totalAmount;
    private SalesPerson salesPerson;

    public SalesPerson getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(SalesPerson salesPerson) {
        this.salesPerson = salesPerson;
    }
    private static int count = 0;
    
    public Order(){
        count++;
        orderNumber= count;
        orderItemList = new ArrayList<OrderItem>();
    }

    public ArrayList<OrderItem> getOrderItemList() {
        return orderItemList;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public void setOrderItemList(ArrayList<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public OrderItem addOrderItem(MarketOffer mo, int q, double price)
    {
        OrderItem orderItem = new OrderItem();
        orderItem.setMarketOffer(mo);
        orderItem.setQuantity(q);
        orderItem.setSalesPrice(price);       
        orderItemList.add(orderItem);
        return orderItem;
    }
    
    public void removeOrderItem(OrderItem oi)
    {
        orderItemList.remove(oi);
    }
    
    public double calculateOrderRevenue(Order order)
    {
        this.totalAmount=0;
        for(OrderItem orderItem:orderItemList)
        {
           this.totalAmount += orderItem.getQuantity() * orderItem.getSalesPrice();
        }
        return totalAmount;
    }

    public double computeCommission() {
        double commission = 0.0;
        for(OrderItem oi : this.orderItemList){
            commission += (oi.getSalesPrice()-oi.getMarketOffer().getTargetPrice())*0.1;
        }
        return commission;
    }
}
