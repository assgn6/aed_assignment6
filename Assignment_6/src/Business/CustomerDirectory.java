/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class CustomerDirectory {
    private ArrayList<Customer> CustomerDirectory;

    public CustomerDirectory() {
       CustomerDirectory = new ArrayList<Customer>();
    }

    public ArrayList<Customer> getCustomerDirectory() {
        return CustomerDirectory;
    }

    public void setCustomerDirectory(ArrayList<Customer> CustomerDirectory) {
        this.CustomerDirectory = CustomerDirectory;
    }
    
    public Customer addCustomer()
    {
        Customer c=new Customer();
        CustomerDirectory.add(c);
        return c;
    }
    
    public void deleteCustomer(Customer c)
    {
        CustomerDirectory.remove(c);
    }
    
    public Customer findCustomer(String keyword)
    {
        for(Customer c:CustomerDirectory)
        {
            if(c.getName().equalsIgnoreCase(keyword))
            {
                return c;
            }
        }
        return null;
    }
    
}
