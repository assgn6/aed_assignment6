/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Comparator;

/**
 *
 * @author sanch
 */
public class Person {
    private String name;
    private ArrayList<UserAccount> userAccount;
    private double revenue;

 
    
    public Person() {
        userAccount = new ArrayList<UserAccount>();
    }

    public ArrayList<UserAccount> getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(ArrayList<UserAccount> userAccount) {
        this.userAccount = userAccount;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String toString()
    {
        return this.name;
    }
    
    public UserAccount addUserAccount(){
        UserAccount ua = new UserAccount();
        userAccount.add(ua);
        return ua;
    }
    public void deleteUserAccount(UserAccount u)
    {
       userAccount.remove(u);
    }
    public double getRevenue() {
        return revenue;
    }

    public void setRevenue(double revenue) {
        this.revenue = revenue;
    }
    public static final Comparator<Person> DECENDING_COM = new Comparator<Person>() {

   

        @Override
        public int compare(Person t, Person t1) {
            return (int) (t1.revenue - t.revenue);
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    };
}
