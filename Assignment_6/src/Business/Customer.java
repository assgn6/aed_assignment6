/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author sanch
 */
public class Customer {
    private String name;
    private String emailId;
    private String contactNo;
    private Market market;
    private ArrayList<Order> orderList;

    public Customer() {
        orderList = new ArrayList<Order>();
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public ArrayList<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(ArrayList<Order> orderList) {
        this.orderList = orderList;
    }
    
    public Order addOrder()
    {
        Order o=new Order();
        orderList.add(o);
        return o;       
    }
    
    public void deleteOrder(Order o)
    {
        orderList.remove(o);
        
    }
    
    @Override
    public String toString()
    {
        return this.name;
    }
}
