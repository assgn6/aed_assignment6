/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author sanch
 */
public class Market {
    private String marketName;
    ArrayList<Customer> customer;
    private double offerPercent;

    public Market() {
        customer = new ArrayList<Customer>();
    }
    
    public double getOfferPercent() {
        return offerPercent;
    }

    public void setOfferPercent(double offerPercent) {
        this.offerPercent = offerPercent;
    }
     
    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public ArrayList<Customer> getCustomer() {
        return customer;
    }

    public Customer addCustomer(){
        Customer c = new Customer();
        customer.add(c);
        return c;
    }
    
    public Customer searchCustomer(String name){
        for(Customer c : customer){
            if(c.getName().equalsIgnoreCase(name)){
                return c;
            }
        }
        return null;
    }
    
    public void removeCustomer(Customer c)
    {
        customer.remove(c);   
    }
    
    public String toString()
    {
        return marketName;
    }
    
}
