/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author kothari
 */
public class MarketOfferCatalog {
    ArrayList<MarketOffer> marketOffers;

    public MarketOfferCatalog() {
        marketOffers = new ArrayList<MarketOffer>();
    }

    public ArrayList<MarketOffer> getMarketOffers() {
        return marketOffers;
    }

    public void setMarketOffers(ArrayList<MarketOffer> marketOffers) {
        this.marketOffers = marketOffers;
    }
    
    public MarketOffer addMarketOffer(Market m, Product p){
        MarketOffer marketOffer = new MarketOffer(m,p);
        marketOffers.add(marketOffer);
        return marketOffer;
    }
    public void removeMarketOffer(MarketOffer marketOffer)
    {
        marketOffers.remove(marketOffer);
    }
     
    public MarketOffer searchMarketOffer(String name,String marketName)
    {
        for(MarketOffer mo : marketOffers){
            if(mo.getP().getProdName().equals(name) && mo.getM().getMarketName().equals(marketName)){
                return mo;
            }
        }
        return null;
    }
    
}
