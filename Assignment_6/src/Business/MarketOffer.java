/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author kothari
 */
public class MarketOffer {
    Market m;
    Product p;
    Double ceilPrice;
    Double floorPrice;
    Double targetPrice;

    public MarketOffer(Market m, Product p) {
        this.m = m;
        this.p = p;
        ceilPrice = p.getCeilprice()+p.getCeilprice()*m.getOfferPercent()/100;
        floorPrice = p.getFloorPrice()+p.getFloorPrice()*m.getOfferPercent()/100;
        targetPrice = p.getTargetPrice()+p.getTargetPrice()*m.getOfferPercent()/100;
    }

    public Market getM() {
        return m;
    }

    public void setM(Market m) {
        this.m = m;
    }

    public Product getP() {
        return p;
    }

    public void setP(Product p) {
        this.p = p;
    }

    public Double getCeilPrice() {
        return ceilPrice;
    }

    public void setCeilPrice(Double ceilPrice) {
        this.ceilPrice = ceilPrice;
    }

    public Double getFloorPrice() {
        return floorPrice;
    }

    public void setFloorPrice(Double floorPrice) {
        this.floorPrice = floorPrice;
    }

    public Double getTargetPrice() {
        return targetPrice;
    }

    public void setTargetPrice(Double targetPrice) {
        this.targetPrice = targetPrice;
    }
    
    @Override
    public String toString()
    {
        return this.p.getProdName();
    }
    
}
