/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author sanch
 */
public class UserAccountDirectory {
    
     private ArrayList<UserAccount> userAccountList;

    public UserAccountDirectory() {
        userAccountList=new ArrayList<UserAccount>();
    }

    public ArrayList<UserAccount> getUserAccountList() {
        return userAccountList;
    }

    public void setUserAccountList(ArrayList<UserAccount> userAccountList) {
        this.userAccountList = userAccountList;
    }

    public UserAccount addUserAccount()
    {
        UserAccount userAccount=new UserAccount();
        userAccountList.add(userAccount);
        return userAccount;
    }
    
    public void deleteUserAccount(UserAccount p)
    {
        userAccountList.remove(p);
    }
    
    public UserAccount findUserAccount(String user)
    {
        for(UserAccount p:userAccountList)
        {
            if(p.getUserName().equalsIgnoreCase(user))
            {
                return p;
            }
        }
        return null;
    }
        
    public UserAccount isValidUser(String username, String password)
    {
        for(UserAccount ua:userAccountList)
        {
            if(ua.getUserName().equals(username) && ua.getPassword().equals(HashPassword.hash(password)))
            {
                return ua;
            }
        }
        return null;
    }
    
    public void removeAccount(String uname)
    {
        for(UserAccount u: userAccountList)
        {

            if(u.getUserName().equalsIgnoreCase(uname))
            {
                userAccountList.remove(u);
                return;
            }
        }
    }
    
}
