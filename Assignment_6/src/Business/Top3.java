/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author kothari
 */
public class Top3 implements Comparable<Top3>{
    
    Product p;
    int count = 0;
    boolean consistent = true;

    public Product getP() {
        return p;
    }

    public void setP(Product p) {
        this.p = p;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isConsistent() {
        return consistent;
    }

    public void setConsistent(boolean consistent) {
        this.consistent = consistent;
    }

    @Override
    public int compareTo(Top3 t) {
        int count1;
        count1 = ((Top3)t).getCount();
        return count1 - count;
    }
    
    
}
