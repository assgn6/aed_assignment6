/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kothari
 */
public class ConfigureBusiness {
    static Business b;

    public ConfigureBusiness() {
        b = new Business();
        loadPerson();
        loadSupplier();
        loadMarket();
        loadSalesPerson();
        loadOrders();
    }
    
    public void loadPerson(){
        try {
            Scanner sc = new Scanner(new File("Person.csv"));
            sc.nextLine();
            Person p = null;
            while(sc.hasNextLine()){
                String[] lineArray = sc.nextLine().split(",");
                p = b.getPersonDirectory().newPerson();
                p.setName(lineArray[0]);
                UserAccount ua = p.addUserAccount();
                ua.setUserName(lineArray[1]);
                ua.setPassword(HashPassword.hash(lineArray[2]));
                ua.setRole(lineArray[3]);

              
                ua.setIsActive(lineArray[4].equals("Active")?Boolean.TRUE:Boolean.FALSE);
                //System.out.println(ua.getIsActive());

                ua.setPerson(p);
                b.getUserAccountDirectory().getUserAccountList().add(ua);
            }
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(ConfigureBusiness.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }
    
    public void loadSupplier(){
        try {
            Scanner sc = new Scanner(new File("Supplier.csv"));
            sc.nextLine();
            Supplier s = null;
            Product product = null;
            while(sc.hasNextLine()){
                String[] lineArray = sc.nextLine().split(",");
                for(Supplier s1: b.getSupplierDirectory().getSupplierlist()){
                    if(s1.getName().equals(lineArray[0])){
                        s=s1;
                        break;
                    }
                }
                if(s == null){
                    s = b.getSupplierDirectory().addSupplier();
                    for(Person p : b.getPersonDirectory().getPersonDirectory()){
                        if(p.getName().equalsIgnoreCase(lineArray[0])){
                            s.setName(p.getName());
                            s.setUserAccount(p.getUserAccount());
                            break;
                        }
                    }
                }
                product = s.getProductCatalog().addProduct();
                product.setProductID(Integer.parseInt(lineArray[1]));
                product.setProdName(lineArray[2]);
                product.setCeilprice(Double.parseDouble(lineArray[3]));
                product.setFloorPrice(Double.parseDouble(lineArray[4]));
                product.setTargetPrice(Double.parseDouble(lineArray[5]));
                product.setAvail(Integer.parseInt(lineArray[6]));
                product.setInitialQuantity(Integer.parseInt(lineArray[6]));
                s=null;
                product = null;
            }
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(ConfigureBusiness.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void loadMarket(){
        Scanner sc;
        try {
            sc = new Scanner(new File("Market.csv"));
            sc.nextLine();
            Market market = null;
            Customer c = null;
            while(sc.hasNextLine()){
                String[] lineArray = sc.nextLine().split(",");
                market = b.getMarketList().searchMarket(lineArray[0]);
                if(market==null){
                    market = b.getMarketList().addMarket();
                    market.setMarketName(lineArray[0]);
                    market.setOfferPercent(Double.parseDouble(lineArray[1]));
                }
                
                c = market.searchCustomer(lineArray[2]);
                if(c==null){
                    c=market.addCustomer();
                    c.setName(lineArray[2]);
                    c.setEmailId(lineArray[3]);
                    c.setContactNo(lineArray[4]);
                    c.setMarket(market);
                }
            }   
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(ConfigureBusiness.class.getName()).log(Level.SEVERE, null, ex);
        }
        for(Market m : b.getMarketList().getMarketList()){
            updateMarketOffer(m);
        }
    }
    
    public void updateMarketOffer(Market m){
        MarketOffer mo = null;
        for(Supplier s : b.getSupplierDirectory().getSupplierlist()){
            for(Product p : s.getProductCatalog().getProductcatalog()){
                    mo = b.getMarketOfferCatalog().addMarketOffer(m, p);
                    p.getMarketOffers().add(mo);
                    //System.out.println(mo.getCeilPrice());
                }
            }
        }
    
    
    public void loadSalesPerson(){
        for(Person p : b.getPersonDirectory().getPersonDirectory()){
            for(UserAccount ua : p.getUserAccount()){
                if(ua.getRole().equalsIgnoreCase("SalesPerson")){
                    SalesPerson s = b.getSalesPersonDirectory().addSalesPerson();
                    s.getUserAccount().add(ua);
                    s.setName(p.getName());
                }
            }
        }
    }
    
    public void loadOrders(){
        Scanner sc;
        Order order = null;
        try {
            sc = new Scanner(new File("Order.csv"));
            sc.nextLine();
            while(sc.hasNextLine()){
                String[] lineArray = sc.nextLine().split(",");
                for(Order o : b.getMasterOrderCatalog().getOrderCatalog()){
                    if(o.getOrderNumber()==Integer.parseInt(lineArray[0])){
                        order = o;
                        break;
                    }
                }
                if(order == null){
                    order = b.getMasterOrderCatalog().addOrder();
                }
                OrderItem oi = new OrderItem();
                Customer c = null;
                for(Market m : b.getMarketList().getMarketList()){
                    c = (m.searchCustomer(lineArray[4]));
                    if(c!=null){
                        order.setCustomer(c);
                        c.getOrderList().add(order);
                        break;
                    }
                }
                oi.setMarketOffer(b.getMarketOfferCatalog().searchMarketOffer(lineArray[1],c.getMarket().getMarketName()));
                //System.out.println(oi.getMarketOffer().getM().getMarketName());
                
                oi.setQuantity(Integer.parseInt(lineArray[2]));
                oi.setSalesPrice(Double.parseDouble(lineArray[3]));
                order.getOrderItemList().add(oi);
                order.setDate(new Date());
                order.setTotalAmount(order.getTotalAmount()+oi.getQuantity()*oi.getSalesPrice());
                //System.out.println(order.getCustomer().getMarket().getMarketName());
                //System.out.println(order.getCustomer().getName());
                SalesPerson sp = b.getSalesPersonDirectory().searchSalesPerson(lineArray[5]);
                order.setSalesPerson(sp);
                if(!sp.getOrders().contains(order))
                    sp.getOrders().add(order);
                sp.setSalesCommission(sp.getSalesCommission()+((oi.getSalesPrice()-oi.getMarketOffer().getTargetPrice())*0.1));
                order = null;
            }
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(ConfigureBusiness.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static Business getB() {
        return b;
    }
    
    
}
