/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Comparator;

/**
 *
 * @author sanchm
 */
public class SalesPerson extends Person {
    
    private double salesCommission = 0;
    

    
    private ArrayList<Order> orders;

    public SalesPerson() {
        orders = new ArrayList<Order>();
    }

    public ArrayList<Order> getOrders() {
        return orders;
    }

    public double getSalesCommission() {
        return salesCommission;
    }

    public void setSalesCommission(double salesCommission) {
        this.salesCommission = salesCommission;
    }
    
    
}

