/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author sanch
 */
public class SalesPersonDirectory {
    private ArrayList<SalesPerson> salesPersonList;

    public SalesPersonDirectory() {
       salesPersonList = new ArrayList<SalesPerson>();  }
    

    public ArrayList<SalesPerson> getSalesPersonList() {
        
        return salesPersonList;
    }
    public SalesPerson addSalesPerson()
    {
        SalesPerson s=new SalesPerson();
        salesPersonList.add(s);
        return s;
        
    }
      public SalesPerson searchSalesPerson(String keyword){
        for (SalesPerson salesPerson : salesPersonList) {
            if(salesPerson.getName().equals(keyword)){
                return salesPerson;
            }
        }
        return null;
    }
      
    public void removeSalesPerson(SalesPerson s)
    {
      salesPersonList.remove(s);
    }
    
}
