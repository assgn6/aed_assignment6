/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author sanch
 */
public class MarketList {
    ArrayList<Market> marketList;

    public MarketList() {
        marketList = new ArrayList<Market>();
    }

    public ArrayList<Market> getMarketList() {
        return marketList;
    }
    
    public Market addMarket()
    {
        Market market = new Market();
        marketList.add(market);
        return market;
        
    }
    
    public Market searchMarket(String marketName){
        for(Market m : marketList)
            if(m.getMarketName().equals(marketName)){
                return m;
            }
        return null;
    }
    public void deleteMarket(Market m)
    {
        marketList.remove(m);
        
    }
    
}
